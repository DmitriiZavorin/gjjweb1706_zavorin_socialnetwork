package com.getjavajob.dzavorin.common.dataSets;

import lombok.*;

import java.sql.Blob;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class Account extends DataSet {

    private String firstName;
    private String secondName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String workPhone;
    private String homeAddress;
    private String workAddress;
    private String email;
    private int icq;
    private String skype;
    private String additionalInfo;
    private String password;
    private byte[] photo;
    @Singular
    private List<String> phones = new ArrayList<>();
    @Singular
    private List<Group> groups = new ArrayList<>();
    @Singular
    private List<Account> friends = new ArrayList<>();

    public Account() {
        super();
    }

    @Builder
    private Account(long id, String firstName, String secondName, String lastName,
                   LocalDate dateOfBirth, String workPhone, String homeAddress,
                   String workAddress, String email, int icq, String skype, String additionalInfo,
                   String password, byte[] photo) {
        super(id);
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.workPhone = workPhone;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.additionalInfo = additionalInfo;
        this.password = password;
        this.photo = photo;
    }
}