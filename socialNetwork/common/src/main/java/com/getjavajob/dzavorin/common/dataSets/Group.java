package com.getjavajob.dzavorin.common.dataSets;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.ArrayList;
import java.util.List;

@Data
public class Group extends DataSet {

    private long adminId;
    private String name;
    private String info;
    @Singular
    private List<Account> subscribers = new ArrayList<>();

    public Group() {
        super();
    }

    @Builder
    public Group(long id, long adminId, String name, String info) {
        super(id);
        this.adminId = adminId;
        this.name = name;
        this.info = info;
    }
}