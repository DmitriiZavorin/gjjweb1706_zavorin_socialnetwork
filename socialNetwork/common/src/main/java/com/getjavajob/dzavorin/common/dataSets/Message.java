package com.getjavajob.dzavorin.common.dataSets;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Message extends DataSet{

    private String text;
    private LocalDate date;
    private Account author;
    private String type;
}