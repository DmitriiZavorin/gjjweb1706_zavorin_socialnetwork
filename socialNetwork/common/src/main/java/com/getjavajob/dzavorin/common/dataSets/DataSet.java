package com.getjavajob.dzavorin.common.dataSets;

public abstract class DataSet {

    private long id;

    protected DataSet() {
    }

    protected DataSet(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}