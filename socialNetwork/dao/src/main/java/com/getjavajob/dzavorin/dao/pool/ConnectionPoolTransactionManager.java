package com.getjavajob.dzavorin.dao.pool;

import com.getjavajob.dzavorin.dao.crud.DAOException;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPoolTransactionManager extends TransactionManager {

    private ThreadLocal<Connection> tl;

    private ConnectionPoolTransactionManager() {
        tl = new ConnectionPoolThreadLocal();

    }

    public static synchronized TransactionManager getInstance() {
        if (tm == null) {
            tm = new ConnectionPoolTransactionManager();
        }
        return tm;
    }

    @Override
    public Connection getConnection() {
        return tl.get();
    }

    @Override
    public void endTransaction() throws DAOException {
        try {
            tl.get().commit();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                tl.get().rollback();
            } catch (SQLException e) {
                throw new DAOException(e);
            } finally {
                ConnectionPool.getInstance().getBack(tl.get());
                tl.remove();
            }
        }
    }

    @Override
    public void shutdown() {
        ConnectionPool.getInstance().shutdown();
        tl.remove();
        tl = null;
        tm = null;
    }
}