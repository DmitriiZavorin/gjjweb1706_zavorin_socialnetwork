package com.getjavajob.dzavorin.dao.pool;

import com.getjavajob.dzavorin.dao.crud.DAOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceTransactionManager extends TransactionManager {

    private DataSource ds;
    private ThreadLocal<Connection> tl;

    private DataSourceTransactionManager() {
      tl = new ThreadLocal<>();
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/social_network");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public static synchronized TransactionManager getInstance() {
        if (tm == null) {
            tm = new DataSourceTransactionManager();
        }
        return tm;
    }

    @Override
    public Connection getConnection() throws DAOException {
        if (tl.get() == null) {
            try {
                tl.set(ds.getConnection());
                tl.get().setAutoCommit(false);
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return tl.get();
    }

    @Override
    public void endTransaction() throws DAOException {
        try {
            tl.get().commit();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                tl.get().rollback();
            } catch (SQLException e) {
                throw new DAOException(e);
            } finally {
                try {
                    tl.get().close();
                } catch (SQLException e) {
                    throw new DAOException(e);
                }
                tl.remove();
            }
        }
    }

    @Override
    public void shutdown() {
        tl = null;
        tm = null;
    }

}
