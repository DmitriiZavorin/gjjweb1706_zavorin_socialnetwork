package com.getjavajob.dzavorin.dao.pool;

import com.getjavajob.dzavorin.dao.crud.DAOException;

import java.sql.Connection;

public abstract class TransactionManager {

    protected static TransactionManager tm = null;

    public abstract Connection getConnection() throws DAOException;

    public abstract void endTransaction() throws DAOException;

    public abstract void shutdown();

    public static synchronized TransactionManager getInstance() {
        return tm;
    }
}
