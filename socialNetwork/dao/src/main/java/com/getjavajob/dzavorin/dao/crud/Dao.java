package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.dataSets.DataSet;
import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;

import java.sql.*;
import java.util.*;

abstract class Dao {

    protected TransactionManager tm;

    protected Dao() {
        this.tm = TransactionManager.getInstance();
    }

    protected abstract <E extends DataSet> E get(long id) throws DAOException;

    protected abstract <E extends DataSet> E createFromResult(ResultSet resultSet) throws DAOException;

    protected boolean deleteById(long id, String query) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public <E extends DataSet> Map<Long, E> getAll(String query) throws DAOException {
        Connection connection = tm.getConnection();
        Map<Long, E> map = new HashMap<>();
        try (ResultSet resultSet = connection.createStatement().executeQuery(query)) {
            while (resultSet.next()) {
                E e = createFromResult(resultSet);
                map.put(e.getId(), e);
            }
            return map;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public <E extends DataSet> E get(long id, String query) throws DAOException {
        Connection connection = tm.getConnection();
        E entity = null;
        try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            prepareStatement.setLong(1, id);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                if (resultSet.next()) {
                    entity = createFromResult(resultSet);
                }
            }
            return entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected <E extends DataSet> List<E> getListId(long id, String query, String column, Dao dao) throws DAOException {
        Connection connection = tm.getConnection();
        List<E> list = new ArrayList<>();
        try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            prepareStatement.setLong(1, id);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                while (resultSet.next()) {
                    list.add(dao.get(resultSet.getLong(column)));
                }
            }
            return list;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}