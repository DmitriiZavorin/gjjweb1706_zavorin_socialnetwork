package com.getjavajob.dzavorin.dao.pool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Deque;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool implements Pool<Connection> {

    private static ConnectionPool cp = null;
    private static final String DEFAULT_PROPERTY_FILE_NAME = "db.properties";
    private static String propertiesFileName = null;
    private int number;
    private final AtomicBoolean isTerminated = new AtomicBoolean(false);
    private final Lock mainLock = new ReentrantLock();
    private final Properties props = new Properties();
    private Deque<Connection> availableCon;
    //private final Queue<Connection> inUseCon = new LinkedBlockingQueue<>();

    public static synchronized ConnectionPool getInstance() {
        if (cp == null) {
            if (propertiesFileName != null) {
                cp = new ConnectionPool(propertiesFileName);  // for the sake of tests!
            } else {
                cp = new ConnectionPool(DEFAULT_PROPERTY_FILE_NAME);
            }
        }
        return cp;
    }

    public static void setPropertiesFileName(String fileName) {
        propertiesFileName = fileName;
    }

    private ConnectionPool(String propertiesFileName) {
        try {
            props.load(this.getClass().getClassLoader().getResourceAsStream(propertiesFileName));
            Class.forName(props.getProperty("db.driver"));
            number = Integer.valueOf(props.getProperty("pool.size"));
            availableCon = new LinkedBlockingDeque<>(number);
            for (int i = 0; i < number; i++) {
                availableCon.add(create());
            }
        } catch (IOException e) {
            throw new ConnectionPoolException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection create() {
        Connection con = null;
        try {
            con = DriverManager.getConnection(
                    props.getProperty("db.url"),
                    props.getProperty("db.user"),
                    props.getProperty("db.password"));
            con.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

    public Connection get() {
        mainLock.lock();
        if (!isTerminated.get()) {
            while (true) {
                if (!availableCon.isEmpty()) {
                    Connection c = availableCon.pollLast();
                    mainLock.unlock();
                    return c;
                }
            }
        } else {
            mainLock.unlock();
            throw new IllegalStateException("Can not retrieve connection, the pool is already terminated");
        }
    }

    public void getBack(Connection c) {
        if (!isTerminated.get()) {
            availableCon.add(c);
        } else {
            try {
                c.close();
            } catch (SQLException e) {
                throw new ConnectionPoolException(e);
            }
        }
    }

    public void shutdown() throws ConnectionPoolException {
        if (cp != null) {
            mainLock.lock();
            isTerminated.set(true);
            try {
                for (Connection c : availableCon) {
                    c.close();
                }
            } catch (SQLException e) {
                throw new ConnectionPoolException(e);
            } finally {
                availableCon.clear();
                cp = null;
                mainLock.unlock();
            }
        }
    }

    public int size() {
        return availableCon.size();
    }

    public int length() {
        return number;
    }
}