package com.getjavajob.dzavorin.dao.pool;

import java.sql.Connection;

public class ConnectionPoolThreadLocal extends ThreadLocal<Connection>{

    @Override
    protected Connection initialValue() {
        return  ConnectionPool.getInstance().get();
    }
}