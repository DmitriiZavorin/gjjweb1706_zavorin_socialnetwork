package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.common.dataSets.DataSet;
import com.getjavajob.dzavorin.common.dataSets.Message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Map;

public class MessageDaoImpl extends Dao {

    private static final String SELECT_ALL = "SELECT * FROM MESSAGES";
    private static final String BY_ID = " WHERE MESSAGE_ID=?";
    private static final String SELECT_WALL_MESSAGES = " WHERE MESSAGE_ID=?";
    private static final String SELECT_BY_ID = SELECT_ALL + BY_ID;

    @Override
    protected <E extends DataSet> E get(long id) throws DAOException {
        return super.get(id, SELECT_BY_ID);
    }

    @Override
    protected Message createFromResult(ResultSet resultSet) throws DAOException {
        try {
            Message message = new Message();
            long id = resultSet.getLong("MESSAGE_ID");
            message.setId(id);
            message.setText(resultSet.getString("TEXT"));
            message.setType(resultSet.getString("TYPE"));
            message.setAuthor(new AccountDaoImpl().get(resultSet.getLong("AUTHOR_ID")));
            String bDate = resultSet.getString("DATE");
            if (bDate != null) {
                message.setDate(LocalDate.parse(bDate));
            }
            return message;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public Map<Long, Account> getAll() throws DAOException {
        return super.getAll(SELECT_ALL);
    }
}