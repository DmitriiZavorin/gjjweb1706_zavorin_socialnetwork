package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.common.dataSets.Group;

import java.sql.*;
import java.util.*;

public class GroupDaoImpl extends Dao {

    private static final String SELECT_ALL = "SELECT * FROM GROUPS";
    private static final String BY_ID = " WHERE GROUP_ID=?";
    private static final String SELECT_BY_ID = SELECT_ALL + BY_ID;
    private static final String SELECT_SUBSCRIBERS_ID = "SELECT DISTINCT ACCOUNT_ID FROM ACCOUNT_GROUPS" + BY_ID;
    private static final String UPDATE_GROUP = "UPDATE GROUPS SET NAME=?, INFO=?, ADMIN_ID=? WHERE GROUP_ID=?";
    private static final String DELETE_BY_ID = "DELETE FROM GROUPS WHERE GROUP_ID=?";
    private static final String DELETE_SUBSCRIBER = "DELETE FROM ACCOUNT_GROUPS WHERE ACCOUNT_ID=? AND GROUP_ID=?";
    private static final String DELETE_GROUP_FROM_ACCOUNTS_BY_GROUP = "DELETE FROM ACCOUNT_GROUPS" + BY_ID;
    private static final String INSERT_ACCOUNT_BY_ID= "INSERT INTO ACCOUNT_GROUPS (ACCOUNT_ID, GROUP_ID) VALUES (?, ?)";
    private static final String INSERT_GROUP = "INSERT INTO GROUPS (ADMIN_ID, NAME, INFO) VALUES (?, ?, ?)";

    @Override
    public Group get(long id) throws DAOException {
        return super.get(id, SELECT_BY_ID);
    }

    public Map<Long, Group> getAll() throws DAOException {
        return super.getAll(SELECT_ALL);
    }

    public Group update(Group group) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(UPDATE_GROUP)) {
            prepareStatement.setString(1, group.getName());
            prepareStatement.setString(2, group.getInfo());
            prepareStatement.setLong(3, group.getAdminId());
            prepareStatement.setLong(4, group.getId());
            prepareStatement.executeUpdate();
            return group;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean delete(Group group) throws DAOException {
        super.deleteById(group.getId(), DELETE_BY_ID);
        return deleteById(group.getId(), DELETE_GROUP_FROM_ACCOUNTS_BY_GROUP);
    }

    public boolean deleteSubscriber(Account account, Group group) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(DELETE_SUBSCRIBER)) {
            prepareStatement.setLong(1, account.getId());
            prepareStatement.setLong(2, group.getId());
            prepareStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public Group insertGroup(Group group) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(INSERT_GROUP, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setLong(1, group.getAdminId());
            prepareStatement.setString(2, group.getName());
            prepareStatement.setString(3, group.getInfo());
            prepareStatement.executeUpdate();
            ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return get(generatedKeys.getLong(1));
            } else {
                throw new SQLException("failed to insert");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public Group updateWithTwoId(long idOne, long idTwo, String query) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
            prepareStatement.setLong(1, idTwo);
            prepareStatement.setLong(2, idOne);
            prepareStatement.executeUpdate();
            return get(idOne);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public Group insertSubscriber(Group group, Account account) throws DAOException {
        return updateWithTwoId(group.getId(), account.getId(), INSERT_ACCOUNT_BY_ID);
    }

    @Override
    protected Group createFromResult(ResultSet resultSet) throws DAOException {
        try {
            Group group = new Group();
            group.setId(resultSet.getLong("GROUP_ID"));
            group.setAdminId(resultSet.getLong("ADMIN_ID"));
            group.setName(resultSet.getString("NAME"));
            group.setInfo(resultSet.getString("INFO"));
            //group.setSubscribers(getSubscribersById(resultSet.getLong("GROUP_ID")));
            return group;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public List<Account> getSubscribers(Group group, AccountDaoImpl accountDao) throws DAOException {
        return getListId(group.getId(), SELECT_SUBSCRIBERS_ID, "ACCOUNT_ID", accountDao);
    }
}