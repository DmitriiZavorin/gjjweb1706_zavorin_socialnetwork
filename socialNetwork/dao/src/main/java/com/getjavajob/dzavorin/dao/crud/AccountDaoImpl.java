package com.getjavajob.dzavorin.dao.crud;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.common.dataSets.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AccountDaoImpl extends Dao {

    private static final String SELECT_ALL = "SELECT * FROM ACCOUNTS";
    private static final String BY_ID = " WHERE ACCOUNT_ID=?";
    private static final String SELECT_BY_ID = SELECT_ALL + BY_ID;
    private static final String SELECT_FRIENDS_BY_ID =
            "SELECT A1.TO_ID, A2.FROM_ID FROM ACCOUNT_FRIENDS A1 " +
                    "LEFT OUTER JOIN ACCOUNT_FRIENDS A2 ON A1.FROM_ID = A2.TO_ID WHERE A1.FROM_ID=? AND A2.TO_ID=? " +
                    "AND A1.CONFIRMED=TRUE AND A2.CONFIRMED=TRUE";
    private static final String SELECT_GROUPS_BY_ID = "SELECT GROUP_ID FROM ACCOUNT_GROUPS" + BY_ID;
    private static final String SELECT_PHONES_BY_ID = "SELECT PHONE FROM ACCOUNT_PHONES" + BY_ID;
    private static final String INSERT_ACCOUNT =
            "INSERT INTO ACCOUNTS (FIRST_NAME, SECOND_NAME, LAST_NAME, BIRTH_DATE, WORK_PHONE, HOME_ADDRESS, " +
                    "WORK_ADDRESS, EMAIL, ICQ, SKYPE, INFO, PASSWORD, PHOTO) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?)";
    private static final String UPDATE_ACCOUNT =
            "UPDATE ACCOUNTS SET FIRST_NAME=?, SECOND_NAME=?, LAST_NAME=?, BIRTH_DATE=?, WORK_PHONE=?, HOME_ADDRESS=?, " +
                    "WORK_ADDRESS=?, EMAIL=?, ICQ=?, SKYPE=?, INFO=?, PHOTO=?, PASSWORD=?" + BY_ID;
    private static final String INSERT_FRIEND =
            "INSERT INTO ACCOUNT_FRIENDS (FROM_ID, TO_ID, CONFIRMED) VALUES (?, ?, ?)";
    //private static final String INSERT_PHONE = "INSERT INTO ACCOUNT_PHONES (ACCOUNT_ID, PHONE) VALUES (?, ?)";
    private static final String VALUES = "(?, ?), ";
    private static final String INSERT_PHONES = "INSERT INTO ACCOUNT_PHONES (ACCOUNT_ID, PHONE) VALUES ";
    private static final String DELETE_PHONES = "DELETE FROM ACCOUNT_PHONES WHERE ACCOUNT_ID=?";
    private static final String DELETE_BY_ID = "DELETE FROM ACCOUNTS WHERE ACCOUNT_ID=?";
    private static final String DELETE_FRIEND = "DELETE FROM ACCOUNT_FRIENDS WHERE (FROM_ID=? AND TO_ID=?) OR " +
            "(FROM_ID=? AND TO_ID=?)";
    private static final String DELETE_GROUP = "DELETE FROM ACCOUNT_GROUPS WHERE (ACCOUNT_ID=? AND GROUP_ID=?)";
    private static final String INSERT_GROUP = "INSERT INTO ACCOUNT_GROUPS (ACCOUNT_ID, GROUP_ID) " +
            "VALUES (?, ?)";

    protected Account createFromResult(ResultSet resultSet) throws DAOException {
        try {
            Account account = new Account();
            long id = resultSet.getLong("ACCOUNT_ID");
            account.setId(id);
            account.setFirstName(resultSet.getString("FIRST_NAME"));
            account.setSecondName(resultSet.getString("SECOND_NAME"));
            account.setLastName(resultSet.getString("LAST_NAME"));
            String bDate = resultSet.getString("BIRTH_DATE");
            if (bDate != null) {
                account.setDateOfBirth(LocalDate.parse(bDate));
            }
            account.setWorkPhone(resultSet.getString("WORK_PHONE"));
            account.setHomeAddress(resultSet.getString("HOME_ADDRESS"));
            account.setWorkAddress(resultSet.getString("WORK_ADDRESS"));
            account.setIcq(resultSet.getInt("ICQ"));
            account.setEmail(resultSet.getString("EMAIL"));
            account.setSkype(resultSet.getString("SKYPE"));
            account.setAdditionalInfo(resultSet.getString("INFO"));
            account.setPassword(resultSet.getString("PASSWORD"));
            account.setPhones(getPhones(account));
            account.setPhoto(resultSet.getBytes("PHOTO"));
            return account;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public Account get(long id) throws DAOException {
        return super.get(id, SELECT_BY_ID);
    }

    public Map<Long, Account> getAll() throws DAOException {
        return super.getAll(SELECT_ALL);
    }

    public List<Account> getFriends(Account account) throws DAOException {
        List<Account> friends = new ArrayList<>();
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(SELECT_FRIENDS_BY_ID)) {
            prepareStatement.setLong(1, account.getId());
            prepareStatement.setLong(2, account.getId());
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                if (resultSet.next()) {
                    friends.add(get(resultSet.getLong("TO_ID")));
                    friends.add(get(resultSet.getLong("FROM_ID")));
                }
            }
            return friends;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public List<String> getPhones(Account account) throws DAOException {
        List<String> phones = new ArrayList<>();
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(SELECT_PHONES_BY_ID)) {
            prepareStatement.setLong(1, account.getId());
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                while (resultSet.next()) {
                    phones.add(resultSet.getString("PHONE"));
                }
            }
            return phones;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public List<Group> getGroups(Account account, GroupDaoImpl groupDao) throws DAOException {
        Connection connection = tm.getConnection();
        return super.getListId(account.getId(), SELECT_GROUPS_BY_ID, "GROUP_ID", groupDao);
    }

    public Account insertAccount(Account account) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(INSERT_ACCOUNT)) {
            prepareStatement.setString(1, account.getFirstName());
            prepareStatement.setString(2, account.getSecondName());
            prepareStatement.setString(3, account.getLastName());
            prepareStatement.setString(4, account.getDateOfBirth() == null ? null : account.getDateOfBirth().toString());
            prepareStatement.setString(5, account.getWorkPhone());
            prepareStatement.setString(6, account.getHomeAddress());
            prepareStatement.setString(7, account.getWorkAddress());
            prepareStatement.setString(8, account.getEmail());
            prepareStatement.setInt(9, account.getIcq());
            prepareStatement.setString(10, account.getSkype());
            prepareStatement.setString(11, account.getAdditionalInfo());
            prepareStatement.setString(12, account.getPassword());
            prepareStatement.setBytes(13, account.getPhoto());
            prepareStatement.executeUpdate();
            insertPhones(account);
            ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                long id = generatedKeys.getLong(1);
                account.setId(id);
                return account;
            } else {
                throw new SQLException("failed to insert");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean insertGroup(Account account, Group group) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(INSERT_GROUP)) {
            prepareStatement.setLong(1, account.getId());
            prepareStatement.setLong(2, group.getId());
            prepareStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean insertFriends(long fromID, long toId, boolean confirmed) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(INSERT_FRIEND)) {
            prepareStatement.setLong(1, fromID);
            prepareStatement.setLong(2, toId);
            prepareStatement.setBoolean(3, confirmed);
            prepareStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean insertPhones(Account account) throws DAOException {
        Connection connection = tm.getConnection();
        List<String> phones = account.getPhones();
        int amount = phones.size();
        if (amount != 0) {
            StringBuilder statement = new StringBuilder(INSERT_PHONES);
            for (String phone : account.getPhones()) {
                statement.append(VALUES);
            }
            statement.deleteCharAt(statement.length() - 1);
            try (PreparedStatement prepareStatement = connection.prepareStatement(statement.toString())) {
                Iterator<String> it = phones.listIterator();
                long id = account.getId();
                for (int i = 1; i < amount * 2; i = i + 2) {
                    prepareStatement.setLong(i, id);
                    prepareStatement.setString(i + 1, it.next());
                }
                prepareStatement.executeUpdate();
                return true;
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return false;
    }

    public Account update(Account account) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement = connection.prepareStatement(UPDATE_ACCOUNT)) {
            prepareStatement.setString(1, account.getFirstName());
            prepareStatement.setString(2, account.getSecondName());
            prepareStatement.setString(3, account.getLastName());
            prepareStatement.setString(4, account.getDateOfBirth() == null ? null : account.getDateOfBirth().toString());
            prepareStatement.setString(5, account.getWorkPhone());
            prepareStatement.setString(6, account.getHomeAddress());
            prepareStatement.setString(7, account.getWorkAddress());
            prepareStatement.setString(8, account.getEmail());
            prepareStatement.setInt(9, account.getIcq());
            prepareStatement.setString(10, account.getSkype());
            prepareStatement.setString(11, account.getAdditionalInfo());
            prepareStatement.setString(13, account.getPassword());
            prepareStatement.setBytes(12, account.getPhoto());
            prepareStatement.setLong(14, account.getId());
            prepareStatement.executeUpdate();
            deletePhones(account);
            insertPhones(account);
            return account;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean deleteGroup(Account account, Group group) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_GROUP)) {
            preparedStatement.setLong(1, account.getId());
            preparedStatement.setLong(2, group.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean deletePhones(Account account) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PHONES)) {
            preparedStatement.setLong(1, account.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean deleteFriends(long fromId, long toId) throws DAOException {
        Connection connection = tm.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FRIEND)) {
            preparedStatement.setLong(1, fromId);
            preparedStatement.setLong(2, toId);
            preparedStatement.setLong(3, toId);
            preparedStatement.setLong(4, fromId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public boolean delete(Account account) throws DAOException {
        return super.deleteById(account.getId(), DELETE_BY_ID);
    }

    public Account getByEmail(String email) throws DAOException {
        Account account = null;
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement =
                     connection.prepareStatement("SELECT ACCOUNT_ID FROM ACCOUNTS WHERE EMAIL=?")) {
            prepareStatement.setString(1, email);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                if (resultSet.next()) {
                    account = get(resultSet.getLong("ACCOUNT_ID"));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return account;
    }

    public Account getByEmailAndPassword(String email, String password) throws DAOException {
        Account account = null;
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement =
                     connection.prepareStatement("SELECT ACCOUNT_ID FROM ACCOUNTS WHERE EMAIL=? AND PASSWORD=?")) {
            prepareStatement.setString(1, email);
            prepareStatement.setString(2, password);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                if (resultSet.next()) {
                    account = get(resultSet.getLong("ACCOUNT_ID"));
                }
            }
            return account;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public List<Long> getBySearchReq(String searchReq) throws DAOException {
        List<Long> result = new ArrayList<>();
        Connection connection = tm.getConnection();
        try (PreparedStatement prepareStatement =
                     connection.prepareStatement("SELECT ACCOUNT_ID FROM ACCOUNTS WHERE FIRST_NAME LIKE ? OR LAST_NAME LIKE ?")) {
            searchReq = "%" + searchReq + "%";
            prepareStatement.setString(1, searchReq);
            prepareStatement.setString(2, searchReq);
            ResultSet resultSet = prepareStatement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getLong("ACCOUNT_ID"));
            }
            return result;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}