package com.getjavajob.dzavorin.dao.pool;

public interface Pool<E> {

    E get();

    void getBack(E e);

}