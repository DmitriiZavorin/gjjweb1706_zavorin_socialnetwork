package com.getjavajob.dzavorin.dao;

import com.getjavajob.dzavorin.dao.pool.ConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ConnectionPoolTest {

    private ConnectionPool pool;
    private Thread[] threads;
    private Random random = new Random();

    @After
    public void closePool() {
        ConnectionPool.setPropertiesFileName(null);
        pool.shutdown();
    }

    @Before
    public void createPool() {
        ConnectionPool.setPropertiesFileName("connectionPoolTest.properties");
        pool = ConnectionPool.getInstance();
        threads = new Thread[30];
    }

    private Thread createAndRunThread() {
        return new Thread(() -> {
            try {
                Connection con = pool.get();
                Thread.sleep(random.nextInt(100));
                pool.getBack(con);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void return_connection_OK() throws SQLException {
        Connection c1 = pool.get();
        Connection c2 = pool.get();
        assertEquals(pool.size(), pool.length() - 2);
        pool.getBack(c1);
        pool.getBack(c2);
        assertEquals(pool.size(), pool.length());
    }

    @Test
    public void reuse_connection_OK() throws SQLException {
        Connection c1 = pool.get();
        pool.getBack(c1);
        Connection c2 = pool.get();
        assertEquals(c1, c2);
        Connection c3 = pool.get();
        assertNotEquals(c2, c3);
        assertNotEquals(c1, c3);
    }

    @Test
    public void work_process_OK() {
        for (int i = 0; i < threads.length; i++) {
            Thread thread = createAndRunThread();
            threads[i] = thread;
            thread.start();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        pool.shutdown();
    }
}
