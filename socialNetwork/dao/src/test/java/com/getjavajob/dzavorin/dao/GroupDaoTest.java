package com.getjavajob.dzavorin.dao;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.common.dataSets.Group;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.dao.crud.GroupDaoImpl;
import com.getjavajob.dzavorin.dao.pool.ConnectionPool;
import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;
import org.apache.commons.io.IOUtils;
import org.junit.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class GroupDaoTest {

    private TransactionManager tm;
    private GroupDaoImpl dao;
    private AccountDaoImpl accountDao;
    private Account accountOne;
    private Account accountTwo;
    private Account accountThree;
    private Group groupOne;
    private Group groupTwo;
    private Group groupThree;

    @After
    public void close() {
        try {
            tm.endTransaction();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        ConnectionPool.getInstance().shutdown();
        tm = null;
    }

    @Before
    public void createPoolAndAccounts() throws DAOException {
        tm = ConnectionPoolTransactionManager.getInstance();
        Connection connection = tm.getConnection();
        try {
            String[] queries = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("insert.sql"), "utf-8").split(";");
            for (String q : queries) {
                connection.createStatement().executeUpdate(q + ";");
            }
            tm.endTransaction();
        } catch (SQLException | IOException | DAOException e) {
            e.printStackTrace();
        }
        dao = new GroupDaoImpl();
        accountDao = new AccountDaoImpl();
        accountOne = Account.builder()
                .id(1)
                .firstName("Ivan")
                .password("qwerty")
                .build();
        accountTwo = Account.builder()
                .id(2)
                .firstName("Dima")
                .password("asdf123")
                .build();
        accountThree = Account.builder()
                .id(3)
                .firstName("Petr")
                .password("567")
                .build();
    }

    @Before
    public void createGroups() {
        groupOne = new Group(1L, 1L, "Cars", "Bla bla bla");
        groupOne.setSubscribers(Arrays.asList(accountOne, accountTwo));
        groupTwo = new Group(2L, 2L, "Airplanes", "ho-ho-ho");
        groupTwo.setSubscribers(Arrays.asList(accountOne, accountTwo));
        groupThree = new Group(3L, 3L, "Submarines", "pum-purum");
        groupThree.setSubscribers(Arrays.asList(accountThree, accountOne));
    }

    @Test
    public void getById_OK() throws DAOException {
        assertEquals("Submarines" , dao.get(3).getName());
    }

    @Test
    public void getById_Fail() throws DAOException {
        assertNull(dao.get(5));
    }

    @Test
    public void getAll_OK() throws DAOException {
        assertEquals(3 , dao.getAll().size());
    }

    @Test
    public void getSubscribers_OK() throws DAOException {
        List<Account> expected = Arrays.asList(accountThree, accountOne);
        assertEquals(expected.size(), dao.getSubscribers(groupThree, accountDao).size());
    }

    @Test
    public void insertSubscriber_OK() throws DAOException {
        dao.insertSubscriber(groupTwo, accountThree);
        assertEquals("Dasha", dao.getSubscribers(groupTwo, accountDao).get(3).getFirstName());
    }

    @Test
    public void update_OK() throws DAOException {
        groupTwo.setName("Insects");
        dao.update(groupTwo);
        assertEquals("Insects", dao.get(2).getName());
    }

    @Test
    public void insertGroup() throws DAOException {
        Group groupFour = Group.builder().adminId(3L).name("Helicopters").info("tututu").build();
        dao.insertGroup(groupFour);
        assertEquals("Helicopters", dao.get(4).getName());
    }

    @Test
    public void delete_OK () throws DAOException {
        dao.delete(groupOne);
        assertNull(dao.get(1));
    }

    @Test
    public void deleteSubscriber_OK () throws DAOException {
        dao.deleteSubscriber(accountOne, groupTwo);
        assertEquals(3, dao.getSubscribers(groupTwo, accountDao).size());
    }
}