package com.getjavajob.dzavorin.dao;

import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.dao.crud.GroupDaoImpl;
import com.getjavajob.dzavorin.dao.pool.ConnectionPool;
import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;
import org.apache.commons.io.IOUtils;
import org.junit.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static java.util.Arrays.*;
import static org.junit.Assert.*;

public class AccountDaoTest {

    private TransactionManager tm;
    private GroupDaoImpl groupDao;
    private AccountDaoImpl dao;
    private Account accountOne;
    private Account accountTwo;
    private Account accountThree;


    @After
    public void close() {
        try {
            tm.endTransaction();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        ConnectionPool.getInstance().shutdown();
        tm = null;
    }

    @Before
    public void createPoolAndAccounts() throws DAOException {
        tm = ConnectionPoolTransactionManager.getInstance();
        Connection connection = tm.getConnection();
        try {
            String[] queries = IOUtils.toString(this.getClass().getClassLoader().getResourceAsStream("insert.sql"), "utf-8").split(";");
            for (String q : queries) {
                connection.createStatement().executeUpdate(q + ";");
            }
            tm.endTransaction();
        } catch (SQLException | IOException | DAOException e) {
            e.printStackTrace();
        }
        dao = new AccountDaoImpl();
        groupDao = new GroupDaoImpl();
        accountOne = Account.builder()
                .id(1)
                .firstName("Ivan")
                .email("ivan56@gmail.com")
                .password("qwerty")
                .build();
        accountOne.setPhones(asList("+71234567890"));
        accountTwo = Account.builder()
                .id(2)
                .firstName("Dima")
                .email("dima78@mail.ru")
                .password("asdf123")
                .build();
        accountTwo.setPhones(asList("+74951234567", "+789636375843"));
        accountThree = Account.builder()
                .id(3)
                .firstName("Petr")
                .email("petrPetrov@yandex.ru")
                .password("567")
                .build();
        accountThree.setPhones(asList("+9012345678"));
    }

    @Test
    public void getById_Exist() throws DAOException {
        Account account = dao.get(1);
        assertEquals("Ivan", account.getFirstName());
    }

    @Test
    public void getById_Fail() throws DAOException {
        Account account = dao.get(6);
        assertNull(account);
    }

    @Test
    public void getAll_OK() throws DAOException {
        Map<Long, Account> accounts = dao.getAll();
        assertEquals(5, accounts.size());
    }

    @Test
    public void getPhones_OK() throws DAOException {
        List<String> expected = asList("+79133456758", "+78963645581", "+79673784719");
        assertEquals(expected, dao.getPhones(accountTwo));
    }

//    @Test
//    public void getGroups_OK() throws DAOException {
//        List<Group> expected = asList(groupOne, groupTwo);
//        assertEquals(expected, dao.getGroups(accountTwo, groupDao));
//    }

    @Test
    public void getFriends_OK() throws DAOException {
        List<Account> expected = asList(accountTwo, accountThree);
        assertEquals(expected.size(), dao.getFriends(accountOne).size());
    }

    @Test
    public void update_OK() throws DAOException {
        accountTwo.setFirstName("Ilya");
        assertEquals("Ilya", dao.update(accountTwo).getFirstName());
    }

    @Test
    public void insertAccount_OK() throws DAOException {
        Account account = Account.builder()
                .firstName("Slava")
                .lastName("Vyacheslavovich")
                .email("slava13@mail.ru")
                .password("123456")
                .build();
        dao.insertAccount(account);
        Account result = dao.get(6);
        assertEquals(result, account);
    }

    @Test
    public void deleteAccount_OK() throws DAOException {
        dao.delete(accountThree);
        assertNull(dao.get(3));
    }

    @Test
    public void insertFriend_OK() throws DAOException {
        dao.insertFriends(2, 3, true);
        List<Account> expected = new ArrayList<>(asList(accountOne, accountThree));
        assertEquals(expected.size(), dao.getFriends(accountTwo).size());
    }
}