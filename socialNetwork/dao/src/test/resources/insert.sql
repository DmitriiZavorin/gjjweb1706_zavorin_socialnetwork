INSERT INTO ACCOUNTS (FIRST_NAME, SECOND_NAME, LAST_NAME, BIRTH_DATE,
                      WORK_PHONE, HOME_ADDRESS, WORK_ADDRESS, EMAIL, ICQ, SKYPE, INFO, PASSWORD, PHOTO)
VALUES ('Ivan', 'Ivanovich', 'Ivanov', '1956-02-23', '+79245678901', 'some work address ivan',
                'some home address ivan', 'ivan56@gmail.com', 1234567, 'ivan56', 'some info Ivan', 'qwerty', NULL),
  ('Dima', 'Dmitrievich', 'Dmitrov', '1978-04-11', '+79871234567', 'some work address dima',
           'some home address dima', 'dima78@mail.ru', 987654, 'dimaDima', 'some info Dima', 'asdf123', NULL),
  ('Petr', 'Petrovich', 'Petrov', '1992-08-30' , '+79564945725', 'some work address petr',
           'some home address petr', 'petrPetrov@yandex.ru', 34534562, 'petrovich92', 'some info Petr', '567', NULL),
  ('Masha', 'Alexandrovna', 'Alexandrova', '1986-03-21', '+79564945725', 'some work address masha',
            'some home address masha', 'mashaSA@ffff.ru', 3542356, 'masha312', 'some info Masha', '123', NULL),
  ('Dasha', 'Alexevna', 'Alexeeva', '1995-07-14', '+79563456436', 'some work address dasha',
            'some home address dasha', 'dasha95@ffff.ru', 3543453, 'dashka95', 'some info Dasha', '54321', NULL);

INSERT INTO ACCOUNT_FRIENDS (FROM_ID, TO_ID, CONFIRMED)
VALUES (1, 2, TRUE), (3, 1, TRUE), (4, 5, TRUE), (3, 2, TRUE), (3, 4, TRUE), (5, 2, TRUE);

INSERT INTO GROUPS (ADMIN_ID, NAME, INFO)
VALUES (1, 'Cars', 'Bla bla bla'), (2, 'Airplanes', 'ho-ho-ho'), (3, 'Submarines', 'pum-purum');

INSERT INTO ACCOUNT_GROUPS (GROUP_ID, ACCOUNT_ID)
VALUES (3, 3), (3, 1), (2, 1), (2, 2), (1, 1), (1, 2), (1, 4), (1, 3), (1, 5), (2, 5), (2, 3);

INSERT INTO ACCOUNT_PHONES (ACCOUNT_ID, PHONE)
VALUES (1, '+71234567890'),
  (1, '+74951234567'),
  (2, '+79133456758'),
  (2, '+78963645581'),
  (3, '+78343637582'),
  (3, '+78678367583'),
  (4, '+78963637584'),
  (5, '+78963637585'),
  (4, '+78963637586'),
  (3, '+78963637588'),
  (2, '+79673784719');