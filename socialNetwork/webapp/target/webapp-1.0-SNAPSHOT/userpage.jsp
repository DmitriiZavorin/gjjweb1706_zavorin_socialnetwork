<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ include file="search.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=200px, initial-scale=1" >
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Login Success Page</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <%--<link rel="stylesheet" type="text/css" href="userpage.css" media="screen"/>--%>
    <%--<script src="userpage.js" type="text/javascript"></script>--%>
</head>
<body>
<c:set var="account" value='${sessionScope.account}'/>
<c:set var="photo" value='${sessionScope.photo}'/>
<c:set var="time" value='${sessionScope.time}'/>
</body>
<div class="container">
    <div class="row">
        <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
            <A href="editUser.jsp" >Edit Profile</A>
            <A href="LogoutServlet">Logout</A>
            <br>
            <p class=" text-info">${time}</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">${account.firstName} ${account.secondName} ${account.lastName}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center">
                            <img alt="User Pic" src="data:image/jpeg;base64,${photo}" class="img-circle img-responsive" /> </div>
                        <%--<img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>--%>
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>${account.dateOfBirth}</td>
                                </tr>
                                <tr>
                                    <td>Home Address</td>
                                    <td>${account.homeAddress}</td>
                                </tr>
                                <tr>
                                    <td>Work Address</td>
                                    <td>${account.workAddress}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><a href="mailto:${account.email}">${account.email}</a></td>
                                </tr>
                                <td>Work Phone</td>
                                <td>${account.workPhone}</td>
                                </tr>
                                <c:forEach var="phone" items="${account.phones}">
                                    </tr>
                                    <td>Mobile Phone</td>
                                    <td>${phone}</td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <td>Skype</td>
                                    <td>${account.skype}</td>
                                </tr>
                                <tr>
                                    <td>ICQ</td>
                                    <td>${account.icq}</td>
                                </tr>
                                <tr>
                                    <td>Info</td>
                                    <td>${account.additionalInfo}</td>
                                </tr>
                                </tbody>
                            </table>
                            <%--<button class="btn btn-lg btn-primary btn-block" type="submit">Groups</button>--%>
                            <a href="#" class="btn btn-primary">Friends</a>
                            <a href="#" class="btn btn-primary">Groups</a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                    <%--<span class="pull-right">--%>
                    <%--<a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>--%>
                    <%--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--%>
                    <%--</span>--%>
                </div>
            </div>
        </div>
    </div>
</div>
</html>