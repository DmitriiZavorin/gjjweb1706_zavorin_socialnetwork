package com.getjavajob.dzavorin.webapp.servlets;

import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.service.SearchService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SearchServlet extends HttpServlet {

    private SearchService searchService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        searchService = new SearchService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search");
        try {
            request.setAttribute("searchResult", searchService.getAccountsBySearchReq(search));
            request.getRequestDispatcher("/searchResult.jsp").forward(request, response);
        } catch (DAOException e) {
            response.getWriter().println("<font color=red>Something gone wrong or nothing found. Please, try again.</font>");
            getServletContext().getRequestDispatcher("/startup.jsp").include(request, response);
            e.printStackTrace();
        }
    }
}