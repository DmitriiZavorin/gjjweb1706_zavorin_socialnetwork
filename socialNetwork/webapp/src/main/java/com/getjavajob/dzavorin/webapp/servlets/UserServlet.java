package com.getjavajob.dzavorin.webapp.servlets;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.service.AccountService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

public class UserServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        accountService = new AccountService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            long id = Long.valueOf(request.getParameter("userId"));
            Account account = accountService.getUserById(id);
            request.setAttribute("account", account);
            request.setAttribute("photo", Base64.getEncoder().encodeToString(account.getPhoto()));
            request.getRequestDispatcher("/user.jsp").forward(request, response);
        } catch (DAOException | NumberFormatException | NullPointerException e) {
            response.getWriter().println("<font color=red>Something gone wrong or nothing found. Please, try again.</font>");
            getServletContext().getRequestDispatcher("/search.jsp").include(request, response);
            e.printStackTrace();
        }
    }
}