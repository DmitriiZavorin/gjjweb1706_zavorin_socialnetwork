package com.getjavajob.dzavorin.webapp.servlets;


import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.DataSourceTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        //ConnectionPoolTransactionManager.getInstance();
        DataSourceTransactionManager.getInstance();
    }

    public void contextDestroyed(ServletContextEvent sce) {
        TransactionManager.getInstance().shutdown();
    }
}