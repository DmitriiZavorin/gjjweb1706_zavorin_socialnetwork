package com.getjavajob.dzavorin.webapp.filters;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthenticationFilter implements Filter {

    public void init(FilterConfig fConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String email = req.getParameter("email");
        String pwd = req.getParameter("password");
        String uri = req.getRequestURI();

        HttpSession session = req.getSession(false);
        boolean loggedIn = session != null && session.getAttribute("account") != null;

        if (loggedIn) {
            if (req.getRequestURI().endsWith("LogoutServlet")) {
                request.getRequestDispatcher("LogoutServlet").forward(req, res);
            } else if (uri.endsWith("LoginServlet")
                    || uri.endsWith("/")
                    || uri.endsWith("/startup.jsp")
                    || uri.endsWith("/registration.jsp")) {
                request.getRequestDispatcher("/userpage.jsp").include(req, res);
            } else {
                chain.doFilter(req, res);
            }
        } else if (isCookieExist(req.getCookies())) {
            session = req.getSession();
            request.getRequestDispatcher("StartupServlet").include(req, res);
        } else if (uri.endsWith("StartupServlet") && email != null && pwd != null) {
            chain.doFilter(req, res);
        } else if (uri.endsWith("/registration.jsp") || uri.endsWith("RegistrationServlet")
                || uri.endsWith("/help.jsp") || uri.endsWith("SearchServlet") || uri.endsWith("UserServlet")) {
            chain.doFilter(req, res);
        } else {
            request.getRequestDispatcher("startup.jsp").forward(req, res);
        }
    }

    public void destroy() {
    }

    private boolean isCookieExist(Cookie[] cookies) {
        if (cookies != null) {
            boolean emailCookieExist = false;
            boolean passwordCookieExist = false;
            for (Cookie cookie : cookies) {
                if ("email".equals(cookie.getName())) {
                    emailCookieExist = true;
                }
                if ("password".equals(cookie.getName())) {
                    passwordCookieExist = true;
                }
            }
            return emailCookieExist && passwordCookieExist;
        } else {
            return false;
        }
    }
}