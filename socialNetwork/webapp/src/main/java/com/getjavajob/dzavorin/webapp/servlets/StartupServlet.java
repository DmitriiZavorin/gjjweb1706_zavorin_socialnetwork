package com.getjavajob.dzavorin.webapp.servlets;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.service.AccountService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

public class StartupServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        accountService = new AccountService();
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        request.getSession(true);
        String email = request.getParameter("email");
        String pwd = request.getParameter("password");
        try {
            Account account = accountService.getByEmailAndPwd(email, pwd);
            if (account != null) {
                if (request.getParameter("rememberMe") != null) {
                    Cookie emailCookie = new Cookie("email", email);
                    Cookie pwdCookie = new Cookie("password", pwd);
                    emailCookie.setMaxAge(10 * 60);
                    pwdCookie.setMaxAge(10 * 60);
                    response.addCookie(emailCookie);
                    response.addCookie(pwdCookie);
                }
                HttpSession session = request.getSession(false);
                session.setAttribute("account", account);
                session.setAttribute("photo", Base64.getEncoder().encodeToString(account.getPhoto()));
                session.setAttribute("time", LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME));
                //request.setAttribute("account", account);
                response.setStatus(HttpServletResponse.SC_OK);
                response.sendRedirect("userpage.jsp");
            } else {
                PrintWriter out = response.getWriter();
                out.println("<font color=red>Either email name or password is wrong.</font>");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                getServletContext().getRequestDispatcher("/startup.jsp").include(request, response);
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        if (request.getAttribute("account") == null && request.getCookies() != null) {
            try {
                String email = null;
                String password = null;
                for (Cookie c : request.getCookies()) {
                    if ("email".equals(c.getName())) {
                        email = c.getValue();
                    }
                    if ("password".equals(c.getName())) {
                        password = c.getValue();
                    }
                }
                Account account = accountService.getByEmailAndPwd(email, password);
                HttpSession session = request.getSession(false);
                session.setAttribute("account", account);
                session.setAttribute("photo", Base64.getEncoder().encodeToString(account.getPhoto()));
                session.setAttribute("time", LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME));
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
        request.getRequestDispatcher("/userpage.jsp").include(request, response);
    }
}