package com.getjavajob.dzavorin.webapp.servlets;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.service.AccountService;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Base64;

@MultipartConfig
public class RegistrationServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        accountService = new AccountService();
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("email");
        String pwd = request.getParameter("password");
        Account account = Account.builder()
                .firstName(request.getParameter("First Name"))
                .secondName(request.getParameter("Second Name"))
                .lastName(request.getParameter("Last Name"))
                .email(email)
                .password(pwd)
                .homeAddress(request.getParameter("Home Address"))
                .workAddress(request.getParameter("Work Address"))
                .workPhone(request.getParameter("Work Phone"))
                .skype(request.getParameter("Skype"))
                .additionalInfo(request.getParameter("Info"))
                .photo(IOUtils.toByteArray(request.getPart("Upload photo").getInputStream()))
                .build();
        try {
            account.setDateOfBirth(LocalDate.parse(request.getParameter("Date Of Birth") == null ? LocalDate.now().toString() : request.getParameter("Date Of Birth")));
            account.setIcq(Integer.valueOf(request.getParameter("ICQ")));
        } catch (DateTimeParseException | NumberFormatException e) {
            e.printStackTrace();
        }
        try {
            accountService.createAccount(account);
            if (account != null) {
                HttpSession session = request.getSession();
                session.setAttribute("account", account);
                session.setAttribute("photo", Base64.getEncoder().encodeToString(account.getPhoto()));
                request.getRequestDispatcher("/userpage.jsp").include(request, response);
            }
        } catch (DAOException e) {
            response.sendRedirect("/registration.jsp");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/userpage.jsp").forward(request, response);
    }
}