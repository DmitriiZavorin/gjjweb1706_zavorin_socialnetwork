package com.getjavajob.dzavorin.webapp.servlets;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.service.AccountService;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Base64;

@MultipartConfig
public class EditUserServlet extends HttpServlet {

    private AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        accountService = new AccountService();
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        Account newAccount = Account.builder()
                .firstName(request.getParameter("First Name"))
                .secondName(request.getParameter("Second Name"))
                .lastName(request.getParameter("Last Name"))
                .homeAddress(request.getParameter("Home Address"))
                .workAddress(request.getParameter("Work Address"))
                .workPhone(request.getParameter("Work Phone"))
                .email(request.getParameter("email"))
                .password(request.getParameter("password"))
                .skype(request.getParameter("Skype"))
                .additionalInfo(request.getParameter("Info"))
                .build();

        Account oldAccount = (Account) request.getSession(false).getAttribute("account");

        newAccount.setId(oldAccount.getId());

        try {
            Part part = request.getPart("Upload photo");
            if (part != null && part.getSize() != 0) {
                newAccount.setPhoto(IOUtils.toByteArray(request.getPart("Upload photo").getInputStream()));
            } else {
                newAccount.setPhoto(oldAccount.getPhoto());
            }
            if (request.getParameter("ICQ") != null) {
                newAccount.setIcq(Integer.valueOf(request.getParameter("ICQ")));
            }
            if (request.getParameter("Date Of Birth") != null && !request.getParameter("Date Of Birth").equals("null")) {
                newAccount.setDateOfBirth(LocalDate.parse(request.getParameter("Date Of Birth") == null ? LocalDate.now().toString() : request.getParameter("Date Of Birth")));
            }
        } catch (DateTimeParseException | NumberFormatException e) {
            e.printStackTrace();
        }

        if (!oldAccount.getEmail().equals(newAccount.getEmail())) {
            request.setAttribute("email", newAccount.getEmail());
        }
        if (!oldAccount.getPassword().equals(newAccount.getPassword())) {
            request.setAttribute("password", newAccount.getPassword());
        }

        try {
            accountService.update(newAccount);
            HttpSession session = request.getSession();
            session.setAttribute("account", newAccount);
            session.setAttribute("photo", Base64.getEncoder().encodeToString(newAccount.getPhoto()));
            response.sendRedirect("userpage.jsp");
        } catch (DAOException e) {
            response.sendRedirect("/registration.jsp");
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/userpage.jsp").forward(request, response);
    }
}