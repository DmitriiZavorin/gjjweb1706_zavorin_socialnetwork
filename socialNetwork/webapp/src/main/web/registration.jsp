<!DOCTYPE html>
<html lang="en">
<%@ page session="false" %>
<%@ include file="search.jsp" %>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Registration></title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg=="
          crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
          crossorigin="anonymous">

    <style>
        body {
            padding-top: 70px;
            /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
        }

        .othertop {
            margin-top: 10px;
        }
    </style>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="js/registration.js" type="text/javascript"></script>
    <![endif]-->

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-10 ">
            <form class="form-horizontal" action="RegistrationServlet" method="post" enctype="multipart/form-data">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Registration form</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="First Name">First Name</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user">
                                    </i>
                                </div>
                                <input id="First Name" name="First Name" type="text" placeholder="First Name"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Second Name">Second Name</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user">
                                    </i>
                                </div>
                                <input id="Second Name" name="Second Name" type="text" placeholder="Second Name"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Last">Last name</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-male" style="font-size: 20px;"></i>
                                </div>
                                <input id="Last" name="Last Name" type="text" placeholder="Last name"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- File Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Upload photo">Upload photo</label>
                        <div class="col-md-4">
                            <input id="Upload photo" name="Upload photo" class="input-file" type="file">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Date Of Birth">Date Of Birth</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-birthday-cake"></i>
                                </div>
                                <input id="Date Of Birth" name="Date Of Birth" type="text" placeholder="YYYY-MM-DD"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input Home Address-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Home Address">Home Address</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-home" style="font-size: 20px;"></i>
                                </div>
                                <input id="Home Address" name="Home Address" type="text" placeholder="Home Address"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>


                    <!-- Text input Work Address-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Work Address">Work Address</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-briefcase" style="font-size: 20px;"></i>
                                </div>
                                <input id="Work Address" name="Work Address" type="text" placeholder="Work Address"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input Work Phone-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Work Phone">Work Phone</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone" style="font-size: 20px;"></i>
                                </div>
                                <input id="Work Phone" name="Work Phone" type="text" placeholder="Work Phone"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input Mobile Phone -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Work Phone">Mobile Phone</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-mobile-phone" style="font-size: 20px;"></i>
                                </div>
                                <input id="Mobile Phone" name="Mobile Phone" type="text" placeholder="Mobile Phone"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <%--<div class="container">--%>
                        <%--<div class="row">--%>
                            <%--<div class="col-md-4">--%>
                                <%--<form>--%>
                                    <%--<div class="form-group multiple-form-group">--%>
                                        <%--<div class="form-group input-group">--%>
                                            <%--<input type="text" name="multiple[]" class="form-control" placeholder="Mobile Phone">--%>
                                            <%--<span class="input-group-btn">--%>
                                                <%--<button type="button" class="btn btn-success btn-add">+</button>--%>
                                            <%--</span>--%>
                                        <%--</div>--%>
                                    <%--</div>--%>
                                <%--</form>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <!-- Text input Email-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Email">Email</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope-o"></i>

                                </div>
                                <input id="Email" name="email" type="text" placeholder="Email"
                                       class="form-control input-md">

                            </div>

                        </div>
                    </div>

                    <!-- Text input Skype -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Skype">Skype</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-skype"></i>

                                </div>
                                <input id="Skype" name="Skype" type="text" placeholder="Skype"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input ICQ-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="ICQ">ICQ</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope-o"></i>

                                </div>
                                <input id="ICQ" name="ICQ" type="text" placeholder="ICQ" class="form-control input-md">
                            </div>
                        </div>
                    </div>

                    <!-- Text input Password-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Password">Password</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </div>
                                <input id="Password" name="password" type="text" class="form-control"
                                       placeholder="Password" required="">
                            </div>
                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Info (max 200 words)">Info (max 200 words)</label>
                        <div class="col-md-4">
                            <textarea class="form-control" rows="6" id="Info (max 200 words)" name="Info"></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-4">
                            <button class="btn btn-success" type="submit"><span
                                    class="glyphicon glyphicon-thumbs-up"></span>Submit
                            </button>
                            <%--<a href="#" class="btn btn-success" type="submit"><span class="glyphicon glyphicon-thumbs-up"></span> Submit</a>--%>
                            <a href="registration.jsp" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove-sign"></span> Clear</a>

                        </div>
                    </div>

                </fieldset>
            </form>
        </div>

        <%--<div class="col-md-2 hidden-xs">--%>
        <%--<img src="http://websamplenow.com/30/userprofile/images/avatar.jpg" class="img-responsive img-thumbnail ">--%>
        <%--</div>--%>

    </div>
</div>
<!-- jQuery Version 1.11.1 -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>

