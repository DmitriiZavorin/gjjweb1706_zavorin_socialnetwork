<%@ page session="false" %>
<%@ include file="search.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=200px, initial-scale=1" >
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SocialNetwork</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="btn-toolbar">
</div>
<div class="well">
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th style="width: 36px;"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="account" items="${searchResult}">
            <tr>
                <td>${account.id}</td>
                <td>${account.firstName}</td>
                <td>${account.lastName}</td>
                <td><a href="mailto:${account.email}">${account.email}</a></td>
                <td>
                    <a href="UserServlet?userId=${account.id}" data-original-title="User" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-user"></i></a>
                    <a href="#" data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>