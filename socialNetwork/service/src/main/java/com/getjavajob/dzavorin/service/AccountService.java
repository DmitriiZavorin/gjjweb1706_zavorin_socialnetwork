package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.dataSets.Group;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.dao.crud.GroupDaoImpl;
import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;
import org.mindrot.jbcrypt.BCrypt;

import java.util.List;

public class AccountService {

    private static final int MAX_GROUPS = 100;
    private static final int MAX_FRIENDS = 1000;

    private final AccountDaoImpl accountDao;
    private final GroupDaoImpl groupDao;
    private final TransactionManager tm;

    public AccountService(AccountDaoImpl accountDao) { // used for tests mock
        this.tm = ConnectionPoolTransactionManager.getInstance();
        this.accountDao = accountDao;
        this.groupDao = new GroupDaoImpl();
    }

    public AccountService() {
        this.tm = TransactionManager.getInstance();
        this.accountDao = new AccountDaoImpl();
        this.groupDao = new GroupDaoImpl();
    }

    public Account createAccount(Account account) throws DAOException {
        try {
            account.setPassword(BCrypt.hashpw(account.getPassword(), BCrypt.gensalt(12)));
            return accountDao.insertAccount(account);
        } finally {
            tm.endTransaction();
        }
    }

    public boolean deleteAccount(Account account) throws DAOException {
        try {
            return accountDao.delete(account);
        } finally {
            tm.endTransaction();
        }
    }

    public Account update(Account account) throws DAOException {
        try {
            account.setPassword(BCrypt.hashpw(account.getPassword(), BCrypt.gensalt(12)));
            return accountDao.update(account);
        } finally {
            tm.endTransaction();
        }
    }

    public boolean addFriend(Account fromAccount, Account toAccount) throws DAOException, AccountServiceException {
            if (fromAccount.getFriends().size() < MAX_FRIENDS && toAccount.getFriends().size() < MAX_FRIENDS) {
                try {
                    return accountDao.insertFriends(fromAccount.getId(), toAccount.getId(), true);
                } finally {
                    tm.endTransaction();
                }
            } else {
                throw new AccountServiceException("Friends limit exceeded");
            }
    }

    public boolean deleteFriend(Account fromAccount, Account toAccount) throws DAOException {
        try {
            return accountDao.deleteFriends(fromAccount.getId(), toAccount.getId());
        } finally {
            tm.endTransaction();
        }
    }

    public List<Account> getFriends(Account account) throws DAOException {
        try {
            return accountDao.getFriends(account);
        } finally {
            tm.endTransaction();
        }
    }

    public List<Group> getGroups(Account account) throws DAOException {
        try {
            return accountDao.getGroups(account, groupDao);
        } finally {
            tm.endTransaction();
        }
    }

    public List<String> getPhones(Account account) throws DAOException {
        try {
            return accountDao.getPhones(account);
        } finally {
            tm.endTransaction();
        }
    }

    public Account getUserByEmail(String email) throws DAOException {
        try {
            return accountDao.getByEmail(email);
        } finally {
            tm.endTransaction();
        }
    }

    public Account getUserById(long id) throws DAOException {
        try {
            return accountDao.get(id);
        } finally {
            tm.endTransaction();
        }
    }

    public Account getByEmailAndPwd(String email, String password) throws DAOException {
        try {
            Account account = accountDao.getByEmail(email);
            if (BCrypt.checkpw(password, account.getPassword())) {
                account.setGroups(accountDao.getGroups(account, groupDao));
                account.setFriends(accountDao.getFriends(account));
                return account;
            }
            return null;
        } finally {
            tm.endTransaction();
        }
    }
}