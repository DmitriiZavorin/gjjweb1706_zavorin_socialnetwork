package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.dao.crud.GroupDaoImpl;
import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;

import java.util.ArrayList;
import java.util.List;

public class SearchService {

    private final AccountDaoImpl accountDao;
    private final GroupDaoImpl groupDao;
    private final TransactionManager tm;

    public SearchService() {
        this.tm = TransactionManager.getInstance();
        this.accountDao = new AccountDaoImpl();
        this.groupDao = new GroupDaoImpl();
    }

    public List<Account> getAccountsBySearchReq(String searchReq) throws DAOException {
        try {
            List<Account> accounts = new ArrayList<>();
            for (Long id : accountDao.getBySearchReq(searchReq)) {
                accounts.add(accountDao.get(id));
            }
            return accounts;
        } finally {
            tm.endTransaction();
        }
    }
}