package com.getjavajob.dzavorin.service;

public class AccountServiceException extends Exception {

    public AccountServiceException(String text) {
        super(text);
    }

    public AccountServiceException(Throwable cause) {
        super(cause);
    }
}