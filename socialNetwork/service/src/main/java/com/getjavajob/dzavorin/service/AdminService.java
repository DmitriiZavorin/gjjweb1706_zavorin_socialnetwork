package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.dao.pool.ConnectionPoolTransactionManager;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AdminService {

    private final AccountDaoImpl accountDao;
    private final TransactionManager tm;

    public AdminService() {
        this.tm = TransactionManager.getInstance();
        this.accountDao = new AccountDaoImpl();
    }

    public List<String> getAccountsShortInfo() throws DAOException {
        List<String> shortInfo = new ArrayList<>();
        for (Account a : accountDao.getAll().values()) {
            String phones = a.getPhones()
                                .stream()
                                .map(Object::toString)
                                .collect(Collectors.joining(", "));
            String accountInfo = a.getFirstName() + " " + a.getLastName() + " " + phones + " " + a.getHomeAddress();
            shortInfo.add(accountInfo);
        }
        return shortInfo;
    }
}