package com.getjavajob.dzavorin.service;

import com.getjavajob.dzavorin.dao.crud.AccountDaoImpl;
import com.getjavajob.dzavorin.common.dataSets.Account;
import com.getjavajob.dzavorin.dao.crud.DAOException;
import com.getjavajob.dzavorin.dao.pool.TransactionManager;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import static java.util.Arrays.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class AccountServiceTest {

    private static AccountService ul;
    private static AccountDaoImpl dao;
    private static TransactionManager tm;
    private Account accountOne;
    private Account accountTwo;
    private Account accountThree;

    @BeforeClass
    public static void createInstances() throws DAOException {
        dao = mock(AccountDaoImpl.class);
        ul = new AccountService(dao);
    }

    @Before
    public void createAccounts() throws DAOException {
        accountOne = Account.builder()
                .id(1)
                .firstName("Ivan")
                .password("qwerty")
                .build();
        accountTwo = Account.builder()
                .id(2)
                .firstName("Petr")
                .password("123asd")
                .build();
        accountThree = Account.builder()
                .id(3)
                .firstName("Nickolai")
                .password("567")
                .build();
    }

    @Test
    public void add_user_OK() throws DAOException {
        when(dao.insertAccount(accountOne)).thenReturn(accountOne);
        assertEquals(accountOne, ul.createAccount(accountOne));
    }

    @Test
    public void delete_user_OK() throws DAOException {
        when(dao.delete(accountOne)).thenReturn(true);
        assertTrue(ul.deleteAccount(accountOne));
    }

    @Test
    public void update_user_OK() throws DAOException {
        when(dao.update(accountOne)).thenReturn(accountOne);
        assertEquals(accountOne, ul.update(accountOne));
    }

    @Test
    public void get_friends_list_OK() throws DAOException {
        accountOne.setFriends(asList(accountTwo, accountThree));
        when(dao.getFriends(accountOne)).thenReturn(asList(accountTwo, accountThree));
        assertEquals(accountOne.getFriends(), ul.getFriends(accountOne));
    }

    @Test
    public void add_freind_OK() throws DAOException, AccountServiceException {
        when(dao.insertFriends(1L, 2L, true)).thenReturn(true);
        assertTrue(ul.addFriend(accountOne, accountTwo));
    }

    @Test
    public void add_freind_FAIL() throws DAOException, AccountServiceException {
        when(dao.insertFriends(2L, 2L, true)).thenReturn(false);
        assertFalse(ul.addFriend(accountOne, accountTwo));
    }

    @Test
    public void delete_freind_OK() throws DAOException, AccountServiceException {
        when(dao.deleteFriends(2L, 3L)).thenReturn(true);
        assertTrue(ul.deleteFriend(accountTwo, accountThree));
    }

    @Test
    public void delete_freind_FAIL() throws DAOException, AccountServiceException {
        when(dao.deleteFriends(2L, 2L)).thenReturn(false);
        assertFalse(ul.deleteFriend(accountTwo, accountTwo));
    }
}